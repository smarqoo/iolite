---
layout: product
languages:
  - en
name: Smart Building Gateway
short_description: Residential building gateway
frontpage: true
features:
  - Energy Management
  - Comfort
  - Security
  - Maintenance-free
  - Touchscreen
  - Easy installation
technologies:
  - EnOcean
  - Wireless M-Bus
  - Weather Service
  - LTE
  - WiFi
  - USB
domains:
  - residential
  - office
  - home
partners:
  - iolite_iq
---

### <i class="fa fa-paper-plane wow bounceIn px-3" data-wow-delay=".1s"/>Building digitalization

The _{{page.name}}_ is a wall-mounted IoT gateway, easy to install in new as well as existing properties. The integrated touch-screen and LTE radio make it work even in buildings with zero IT infrastructure.

### <i class="fas fa-leaf wow bounceIn px-3" data-wow-delay=".1s"/>Sustainable and maintenance-free

The _{{page.name}}_ works perfectly with energy-autonomous EnOcean components:

- heating valves harvest energy from the heat of the heaters
- rocker switches transform the mechanical energy of a button press
- room sensors gather energy using solar panels

### <i class="fa fa-wrench wow bounceIn px-3" data-wow-delay=".1s"/>Installation in 40 minutes

Thanks to logistics tailored to IoT requirements, the customization of the _{{page.name}}_ intallation packages to the customer's needs is already part of the production process. This way the installation time on-premise is reduced to a minimum. For example, the setup of the gateway in a typical, existing residentail apartments takes around 40-60 minutes.

The gateway is installed on an existing lights switch. The setup requires no programming skills and is carried out by electricians after a very short training.

### <i class="fa fa-tachometer wow bounceIn px-3" data-wow-delay=".1s"/>Smart Metering Ready

The integrated Wireless M-Bus radio module guarantees compatibility with the _open metering system_ (OMS). If required, the IOLTIE GmbH provides a secure backend infrastructure and API access for the smart metering data transport.

### <i class="fa fa-rocket wow bounceIn px-3" data-wow-delay=".1s"/>Future-proof

<p>The {% include product-link.html product_name="IOLITE Platform" %} of the <i>{{page.name}}</i> assures an efficient installation, setup and operations (including remote management and over-the-air-updates) of the gateway on a mass scale.</p>

The gateway benefits from continuous updates of the IOLITE Plattform, automatically distributed via over-the-air update infrastructure provided by IOLITE.

### <i class="fa fa-handshake wow bounceIn px-3" data-wow-delay=".1s"/>Contact for project development

For more information please refer to [IOLITE IQ GmbH](https://iolite-iq.de).
