---
layout: product
languages:
  - de
name: Smart Building Console
short_description: Universelle Gebäudesteuerung
frontpage: false
features:
  - Visualisierung
  - Data Analytics
  - Automationsregeln
  - Dashboard
technologies:
  - Native App
  - Web
  - Mobile
  - Tablet
domains:
  - home
  - residential
  - office
  - city
---

Die universelle Benutzerschnittstelle der IOLITE Plattform sorgt für eine intuitive Bedienung sogar in den komplexesten Installationen.

### <i class="fas fa-tachometer-alt fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Dashboard

Komfortabler Zugriff auf alle relevanten Informationen der vernetzten Umgebung - Sensoren, Geräte, Räume, Automationsregeln. Ein anpassbares, übersichtliches Dashboard präsentiert die wichtigsten Erkenntnise, damit der Zustand der vernetzten Umgebung mit einem Blick erkennbar ist.

<img src="{{ site.baseurl }}/img/portfolio/products/{{page.name}}/dashboard.png" class="img-fluid py-3"  alt="{{page.name}}" />

### <i class="fas fa-chart-bar fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Datenanalyse

Die Benutzerschnittstelle ist an die Data Analytics Engine der IOLITE Plattform angebunden und verfügt über Widgets, die Datenverläufe und aus den Daten abgeleitete Erkenntnisse visualisieren.

<img src="{{ site.baseurl }}/img/portfolio/products/{{page.name}}/data-analytics.png" class="img-fluid py-3"  alt="{{page.name}}" />

### <i class="fas fa-sliders-h fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Steuerung und Konfiguration

Individuelle oder Gruppen-basierte (Über-)Steuerung aller Geräte. Programmierung von Gruppen, Szenen und Automationsregeln.

### <i class="fas fa-bell fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Benachrichtigung

Automatisierte Benachrichtungen über detektierte Probleme (z.B. Verbindungsprobleme, niedrige Batteriezustände oder sonsige Gerätefehler)

### <i class="fas fa-mobile-alt fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Apps

Komfortabler Zugriff mithilfe von nativen Smartphone Apps, zu Hause und von unterwegs.

<img src="{{ site.baseurl }}/img/portfolio/products/{{page.name}}/apps.png" class="img-fluid py-3"  alt="{{page.name}}" />

### <i class="fas fa-calendar-check fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Reporting

Generierung von Berichten aus aktuellen, historischen und aggregierten Daten.

### <i class="fas fa-puzzle-piece fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Erweiterungen

Leichte Erweiterung des Systems mit neuen Geräten/Sensoren oder Funktionalitäten mittels Over-The-Air-Update.
