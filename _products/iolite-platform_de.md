---
layout: product
languages:
  - de
name: IOLITE Platform
short_description: Interoperable IoT Plattform
frontpage: true
features:
  - Energiemanagement
  - Komfort
  - Sicherheit
  - Interoperabilität
  - App mit Fernzugriff
  - Sprachsteuerung
technologies:
  - EnOcean
  - KNX
  - HomeMatic
  - Philips hue
  - Netatmo
  - Nuki
  - OpenWeatherMap
  - Wireless M-Bus
  - Z-Wave
  - BACnet
  - D-Link
  - digitalSTROM
  - Miele@Home
  - Nest
domains:
  - home
  - residential
  - office
  - city
---

Das Kernprodukt der {{site.data.company.name}} ist die gleichnamige Software-Plattform, die Smart Home- und Smart Building Standards domänenübergreifend vereint.

## <i class="{{ site.data.usps.universal.icon }} fa-fw wow bounceIn py-2" data-wow-delay=".1s"></i> {% t 'usp.universal.title' %}

{% t 'usp.universal.description' %}

<img src="{{ site.baseurl }}/img/portfolio/products/{{page.name}}/main.jpg" class="img-fluid py-3"  alt="{{page.name}}" />

## <i class="{{ site.data.usps.smart.icon }} fa-fw wow bounceIn py-2" data-wow-delay=".1s"></i> {% t 'usp.smart.title' %}

{% t 'usp.smart.description' %}

## <i class="{{ site.data.usps.interoperability.icon }} fa-fw wow bounceIn py-2" data-wow-delay=".1s"></i> {% t 'usp.interoperability.title' %}

{% t 'usp.interoperability.description' %}

## <i class="{{ site.data.usps.security.icon }} fa-fw wow bounceIn py-2" data-wow-delay=".1s"></i> {% t 'usp.security.title' %}

{% t 'usp.security.description' %}

## <i class="{{ site.data.usps.scalability.icon }} fa-fw wow bounceIn py-2" data-wow-delay=".1s"></i> {% t 'usp.scalability.title' %}

{% t 'usp.scalability.description' %}

Typische Anwendungsdomänen der IOLITE Plattform sind:

<ul class="fa-ul">
  <li class="py-1"><i class="fa-li {{site.data.domains.home.icon}}"></i>{% t 'portfolio.domains.home' %}</li>
  <li class="py-1"><i class="fa-li {{site.data.domains.residential.icon}}"></i>{% t 'portfolio.domains.residential' %}</li>
  <li class="py-1"><i class="fa-li {{site.data.domains.office.icon}}"></i>{% t 'portfolio.domains.office' %}</li>
  <li class="py-1"><i class="fa-li {{site.data.domains.city.icon}}"></i>{% t 'portfolio.domains.city' %}</li>
</ul>

Die {{site.data.company.name}} verfügt über ein eigenes, erfahrenes Full-Stack Softwareentwicklungsteam. Wir entwickeln unsere Technologien ständig weiter und arbeiten mit unseren Partnern an neuen Anwendungsszenarien und Funktionen.

## <i class="fas fa-microchip fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Hardware-unabhängig

Die {{page.name}} wurde auf unterschiedliche Hardware-Gateways portiert (von ARM-SoCs bis zu virtualisierten Umgebungen), um den Aufbau spezialisierter Produkte zu ermöglichen. Sollten unsere Gateways Ihre Anforderungen nicht erfüllen, können wir uns schnell an neue Hardware anpassen und skalieren.

## <i class="fas fa-server fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> IoT Backend

Die IOLITE Software ist auf _Edge-Computing_ ausgelegt - alle Use Cases und Kernfunktionen werden lokal, auf dem Gateway umgesetzt. Für den Betrieb der Plattform bieten wir eine komplette _Internet of Things_ (IoT) Infrastruktur:

<ul class="fa-ul">
  <li class="py-1"><i class="fa-li fas fa-boxes"></i>Asset Management</li>
  <li class="py-1"><i class="fa-li fas fa-bell"></i>Monitoring & Alarmierung</li>
  <li class="py-1"><i class="fa-li fas fa-sync"></i>Update Server</li>
  <li class="py-1"><i class="fa-li fas fa-headset"></i>Helpdesk</li>
  <li class="py-1"><i class="fa-li fas fa-clipboard-list"></i>Log & Configuration Storage</li>
  <li class="py-1"><i class="fa-li fas fa-mobile-alt"></i>Remote Access</li>
  <li class="py-1"><i class="fa-li fas fa-code-branch"></i>API Hosting für Partner- und Zusatzdienste (z.B. Wetterinformationen, Verkehrsinfos)</li>
</ul>

Alle unsere Server werden in Deutschland betrieben. Bei Bedarf können wir unsere Infrastruktur beim Kunden hosten.

## <i class="fas fa-handshake fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> B2B2C Kooperation

Die {{page.name}} bildet die technologische Basis für vielfältige Smart Home und Smart Building Lösungen. Die {{site.data.company.name}} kombiniert die innovative Plattformtechnologie mit ergänzenden Entwicklungs- und Serviceleistungen und ist daher der perfekte Partner im Business-to-Business-to-Consumer (B2B2C) Modell.
