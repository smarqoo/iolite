---
layout: product
languages:
  - de
name: Smart Building Gateway
short_description: Digitale Vernetzungslösung für Immobilien
frontpage: true
features:
  - Energiemanagement
  - Komfort
  - Sicherheit
  - Wartungsfrei
  - Installierbar in Bestandsimmobilien
  - Touch-Bedienung
technologies:
  - EnOcean
  - Wireless M-Bus
  - Wetterdienst
  - LTE
  - WiFi
  - USB
domains:
  - residential
  - office
  - home
partners:
  - iolite_iq
---

### <i class="fa fa-paper-plane wow bounceIn px-3" data-wow-delay=".1s"/>Digitalisierung von Bestandsimmobilien

Das _{{page.name}}_ ist ein Wand-montiertes IoT Gateway, perfekt einsetzbar in:

- Bestandsimmobilien
- Neubauten
- Büros
- Öffentlichen Einrichgtunen

Dank des integrierten Touch-Screens und LTE Moduls eignet sich das Gateway für Installationen in Bestandsimmobilien mit keiner IT Infrastruktur.

### <i class="fas fa-leaf wow bounceIn px-3" data-wow-delay=".1s"/>Nachhaltig und wartungsfrei

Kombiniert mit Energie-autarken EnOcean Komponenten emöglicht das _{{page.name}}_ eine Nachrüstung ohne Batterien:

- Heizantriebe gewinnen ihre Energie aus dem warmen Wasser des Heizkörpers
- Lichtschalter erzeugen Funksignale beim mechanischen Tastenruck
- Raumsensoren laden sich mithilfe von Solarzellen auf

### <i class="fa fa-wrench wow bounceIn px-3" data-wow-delay=".1s"/>Installation in 40 Minuten

Gestützt von einem digitalen Logistikprozess wird das Gateway bereits in der Produktion für das Objekt vorkonfiguriert. Die Installationszeit in der Immobilie wird dadurch aufs Minimum reduziert und dauert in einer Bestandsimmobilie typischerweise 40 bis 60 Minuten.

Das Gateway wird auf einen bestehenden Licht- oder Jalousieschalter montiert. Die Installation erfordert keine IT Kenntnisse und wird von Elektrofachkräften nach einer sehr kurzen Schulung vorgenommen.

### <i class="fa fa-tachometer-alt wow bounceIn px-3" data-wow-delay=".1s"/>Smart Metering Ready

Das integrierte Wireless M-Bus Modul ermöglicht die Umsetzung von Smart Metering Szenarien. Die {{site.data.company.name}} stellt eine abgesicherte Backendinfrastruktur für den Transport der Daten bereit.

### <i class="fa fa-rocket wow bounceIn px-3" data-wow-delay=".1s"/>Zukunftssicher

<p>Die {% include product-link.html product_name="IOLITE Platform" %} garantiert eine effiziente Installation, Inbetriebnahme und den zukunftssicheren Betrieb inklusive Remote- und Update Management des Gateways, skalierbar für Massenmarkteinsatz.</p>

Das Gateway profitiert automatisch von neuen Funktionen der IOLITE Plattform, die als over-the-air Update installiert werden.

### <i class="fa fa-handshake wow bounceIn px-3" data-wow-delay=".1s"/>Vertrieb & Projektierung

Für mehr Informationen kontaktieren Sie bitte die [IOLITE IQ GmbH](https://iolite-iq.de).
