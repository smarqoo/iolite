---
layout: product
languages:
  - en
name: IOLITE Platform
short_description: Interoperable IoT platform
frontpage: true
features:
  - Energy management
  - Comfort
  - Security
  - Interoperability
  - App
technologies:
  - EnOcean
  - KNX
  - HomeMatic
  - Philips hue
  - Netatmo
  - Nuki
  - OpenWeatherMap
  - Wireless M-Bus
  - Z-Wave
  - BACnet
  - D-Link
  - digitalSTROM
  - Miele@Home
  - Nest
domains:
  - home
  - residential
  - office
  - city
---

The central product of the {{site.data.company.name}} is the {{page.name}}, an interoperable smart home and smart building middleware.

## <i class="{{ site.data.usps.universal.icon }} fa-fw wow bounceIn py-2" data-wow-delay=".1s"></i> {% t 'usp.universal.title' %}

{% t 'usp.universal.description' %}

<img src="{{ site.baseurl }}/img/portfolio/products/{{page.name}}/main.jpg" class="img-fluid py-3"  alt="{{page.name}}" />

## <i class="{{ site.data.usps.smart.icon }} fa-fw wow bounceIn py-2" data-wow-delay=".1s"></i> {% t 'usp.smart.title' %}

{% t 'usp.smart.description' %}

## <i class="{{ site.data.usps.interoperability.icon }} fa-fw wow bounceIn py-2" data-wow-delay=".1s"></i> {% t 'usp.interoperability.title' %}

{% t 'usp.interoperability.description' %}

## <i class="{{ site.data.usps.security.icon }} fa-fw wow bounceIn py-2" data-wow-delay=".1s"></i> {% t 'usp.security.title' %}

{% t 'usp.security.description' %}

## <i class="{{ site.data.usps.scalability.icon }} fa-fw wow bounceIn py-2" data-wow-delay=".1s"></i> {% t 'usp.scalability.title' %}

{% t 'usp.scalability.description' %}

Traditional applications domains for the {{page.name}} are:

<ul class="fa-ul">
  <li class="py-1"><i class="fa-li {{site.data.domains.home.icon}}"></i>{% t 'portfolio.domains.home' %}</li>
  <li class="py-1"><i class="fa-li {{site.data.domains.residential.icon}}"></i>{% t 'portfolio.domains.residential' %}</li>
  <li class="py-1"><i class="fa-li {{site.data.domains.office.icon}}"></i>{% t 'portfolio.domains.office' %}</li>
  <li class="py-1"><i class="fa-li {{site.data.domains.city.icon}}"></i>{% t 'portfolio.domains.city' %}</li>
</ul>

Our experienced software development team continuously extends the platform technology with new functions and use cases required by our partners.

## <i class="fas fa-microchip fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Hardware-agnostic

The {{page.name}} runs on a variety of hardware gateways (starting with ARM-SoCs to virtualized containers). The high level of compatibility assures easy deployment to new hardware platforms, in case a speciailized gateway is needed to fulfill the customer's requirements.

## <i class="fas fa-server fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> IoT Backend

The {{page.name}} is an _edge-computing_ technology - it works on premise, without the need for an internet connection to fulfill its functionality. However, of course we provide a fully-fledged _Internet of Things_ infrastructure to operate IOLITE-based products in the field:

<ul class="fa-ul">
  <li class="py-1"><i class="fa-li fas fa-boxes"></i>Asset Management</li>
  <li class="py-1"><i class="fa-li fas fa-bell"></i>Monitoring & alerting</li>
  <li class="py-1"><i class="fa-li fas fa-sync"></i>Update server</li>
  <li class="py-1"><i class="fa-li fas fa-headset"></i>Helpdesk</li>
  <li class="py-1"><i class="fa-li fas fa-clipboard-list"></i>Log & Configuration Storage</li>
  <li class="py-1"><i class="fa-li fas fa-mobile-alt"></i>Remote Access</li>
  <li class="py-1"><i class="fa-li fas fa-code-branch"></i>API hosting for partners and 3rd-party services (z.B. weather, public transportation schedule)</li>
</ul>

All our servers are hosted in Germany. If needed, we can setup the infrastructure in the customer's premises.

## <i class="fas fa-handshake fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> B2B2C Cooperation

The {{page.name}} is the foundation of our smart home and smart building solutions. The {{site.data.company.name}} combines the innovative technology with IoT platform services and thus is the perfect partner for business-to-business-to-consumer (B2B2C) cooperations.
