---
layout: product
languages:
  - en
name: Smart Building Console
short_description: Universal building automation solution
frontpage: false
features:
  - Visualisation
  - Data Analytics
  - Automation
  - Dashboard
technologies:
  - Native App
  - Web
  - Mobile
  - Tablet
domains:
  - home
  - residential
  - office
  - city
---

The user interface of the IOLITE Plattform provides intuitive access even to the most complex smart spaces deployments.

### <i class="fas fa-tachometer-alt fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Dashboard

Comfortable access to all relevant information of the connected envrinment - sensors, devices, rooms, automation rules.

<img src="{{ site.baseurl }}/img/portfolio/products/{{page.name}}/dashboard.png" class="img-fluid py-3"  alt="{{page.name}}" />

### <i class="fas fa-chart-bar fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Data analytics

The data analytics engine of the IOLITE platform generates charts and presents important insights, making the state of the environment visible at a glance.

<img src="{{ site.baseurl }}/img/portfolio/products/{{page.name}}/data-analytics.png" class="img-fluid py-3"  alt="{{page.name}}" />

### <i class="fas fa-sliders-h fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Control and configuration

Controls for individual or grouped devices. Configuration of groups, scenes and automation rules.

### <i class="fas fa-bell fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Notifications

Configured or automated notifications about detected important events, custom rules and problems (e.g. connectivitiy issues, low battery states and other device errors).

### <i class="fas fa-mobile-alt fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Apps

The user interface is embedded in native apps, providing comfortable interaction both on-premise and from remote.

<img src="{{ site.baseurl }}/img/portfolio/products/{{page.name}}/apps.png" class="img-fluid py-3"  alt="{{page.name}}" />

### <i class="fas fa-calendar-check fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Reporting

Extraction of reports about current, historical and aggregated data.

### <i class="fas fa-puzzle-piece fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Extensions

The extensions of the IOLITE platform are managed in a dedicated, easy-to-use user interface section, giving the user control over the functions of the system.
