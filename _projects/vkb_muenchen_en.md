---
layout: project
languages:
  - en
name: VKB Showroom
short_description: IoT showroom of Versicherungskammer Bayern
city: München
map: Muenchen
year: 2018
frontpage: false
partners:
  - vkb
  - cl
  - kimocon
products:
  - IOLITE Platform
  - Smart Building Console
technologies:
  - HomeMatic
  - EnOcean
  - KNX
  - Z-Wave
  - Philips hue
  - Netatmo
  - Nuki
  - Miele@Home
  - Nest
  - D-Link
domains:
  - home
---

### <i class="fas fa-paper-plane wow bounceIn p-2" data-wow-delay=".1s"/>Smart home at your fingertips

The goals of the project were:

- make the digital, connected living tangible to employees, customers and partners of the {{site.data.partners.vkb.name}}
- present how digitalization and intelligent use of technology transform and improve the quality of everyday life

The showroom covers a broad range of smart home use cases and is used for training, presentations and meetings with both, employees and external partners.

### <i class="fas fa-sitemap wow bounceIn p-2" data-wow-delay=".1s"/>Infrastructure

<p>The showroom presents the full digitalization potential in more than 20 smart home scenarios, covering comfort, health, security and safety, energy management and ambient assisted living. The open and interopable {% include product-link.html product_name="IOLITE Platform" %} – the technological foundation of the showroom - seamlessly connects devices of different manufacturers and creates a holistic user experience.</p>

<p>The IOLITE middleware uses <strong>11 protocols and APIs</strong> to connects <strong>33 devices and sensors</strong> with a sum of <strong>181 data points</strong>. Despite the technological complexity, the showroom can be controlled in a comfortable and intuitive manner, thanks to the {% include product-link.html product_name="Smart Building Console" %}.</p>

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/kitchen.jpg" class="img-fluid"  alt="{{page.name}}" />

### <i class="fas fa-rocket wow bounceIn p-2" data-wow-delay=".1s"/>Use Cases

The showroom is arranged in typical living areas:

<ul class="fa-ul">
  <li class="py-1"><i class="fa-li fas fa-door-open"></i>Entrance - a contact sensor monitors the entrance door. The user is warned if the door is open for too long or not properly closed. A smart lock automatically closes when users leave home. The state of the door can be checked by talking to Alexa, which can also close the lock, if requested.</li>
  <li class="py-1"><i class="fa-li fas fa-tv"></i>Living room and workspace - lighting scenarios, voice control and preconfigured scenes increase the user's comfort. Presence detectors turn off unused devices upon detected inactivity. During the user's absence detected movement raise burglar alarms.</li>
  <li class="py-1"><i class="fa-li fas fa-blender"></i>Kitchen - a voice warns users if the fridge door is open for too long. Brewing coffee is detected by measuring the power consumption and users are notified when the coffee is ready. If requested, family members can be informed about the routine events in the daily schedule of their loved ones. Alarm is raised if water is detected by the water leakage sensor. The kitchen furniture adapts its height to user preferences and assures optimal cookting conditions.</li>
  <li class="py-1"><i class="fa-li fas fa-utensils"></i>Dining - Movement sensors and wall-mounted rocker switches control predefined scenes, e.g. starting a candle-light dinner scenario.</li>
  <li class="py-1"><i class="fa-li fas fa-bed"></i>Bedroom - <i>Good morning</i> and <i>Sleep well</i> routines control lights and devices. CO<sub>2</sub> sensors warn about bad air quality and recommend when to open windows.</li>
</ul>
