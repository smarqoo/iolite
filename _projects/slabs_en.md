---
layout: project
languages:
  - en
name: S-Labs
short_description: Connected, creative workspace
city: Istanbul
year: 2018
frontpage: true
products:
  - IOLITE Platform
  - Smart Building Console
technologies:
  - EnOcean
  - KNX
  - Miele@Home
  - Weather Service
domains:
  - office
---

_{{page.name}}_ Istanbul is a creative office environment with 22 rooms spanning over 750m².

The office consists of workspaces, meeting rooms, technology testbeds as well as a terrace, kitchen and an interactive presentation floor.

## Use Cases

The goal of the project was to provide a comfortable, energy efficient and secure working environment.

### <i class="fas fa-lightbulb fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Creative environment

- Monitoring of air quality
- Temperature optimization
- Intelligent lights signalling air quality and other events

### <i class="fas fa-leaf fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Energy efficiency

- Monitoring of energy usage
- Automated control
- Heating / cooling optimization

### <i class="fas fa-lock fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Security

- Presence monitoring
- Anomaly detection
- Automatic warnings / alarms

## Infrastructure

_{{page.name}}_ has been equipped with a wide range of sensors and devices, the most based on KNX and EnOcean technologies.

<p>The {% include product-link.html product_name="IOLITE Platform" %} is running on a virtual machine on-premise.</p>

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/meeting.jpg" class="img-fluid py-1"  alt="{{page.name}}" />

### Sensors

Every room is equipped with a wide range of sensors enabling a holistic perception of the current working conditions:

- CO<sub>2</sub>
- humidity
- temperature
- luminosity
- movement in every room

Further door and window sensors are installed for presence and security monitoring. Smart meters provide transparency with regards to energy usage and costs.

### Devices

- Lights including LED
- Cooling and air conditioning
- Heating
- Kitchen appliances
