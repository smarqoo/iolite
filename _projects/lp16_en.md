---
layout: project
languages:
  - en
name: LP16
short_description: Smart office in the heart of Berlin
city: Berlin
year: 2019
frontpage: false
partners:
  - iolite_iq
  - synvia
products:
  - IOLITE Platform
  - Smart Building Gateway
  - Smart Building Console
technologies:
  - EnOcean
  - KNX
  - Weather Service
  - WiFi
domains:
  - office
---

The goal of the _LP16_ project was the digitalization of the {{site.data.partners.synvia.name}}'s representative offices at Leipziger Platz 16 in Berlin. The project was a cooperation of IOLTIE GmbH and [{{site.data.partners.iolite_iq.name}}]({{site.data.partners.iolite_iq.web}}){:target="\_blank"}.

### <i class="fas fa-leaf fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Sustainability goals

The main focus of the project was a verifiable energy usage optimization, primarily in terms of heating efficiency.

### <i class="fas fa-ruler-combined fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Infrastructure planning

The office area features two symmetrical wings. To compare the energy efficiency gains, one of the wings was equipped with smart technology, while the other was left in its original state as reference.

The lighting, HVAC and smart meters were connected with a KNX bus.

A sustainable and maintenance-free installation was achieved by using energy-autonomous sensors and devices:

- heating valves harvest energy from the heat of the heaters
- rocker switches transform the mechanical energy of a button press
- room sensors gather energy using solar panels

<p>The central point of the installation is the {% include product-link.html product_name="IOLITE Platform" %}, which integrated all components and systems. The software runs on a {% include product-link.html product_name="Smart Building Gateway" %} installed in one of the meeting rooms.</p>

### <i class="fas fa-rocket fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Use Cases

<ul class="fa-ul">
  <li class="py-1"><i class="fa-li fas fa-thermometer-half"></i>Room heating automation based on automatic heating plans per room and day of week</li>
  <li class="py-1"><i class="fa-li fas fa-door-open"></i>Heating inactivation when windows are open or no human activity is detected in the room</li>
  <li class="py-1"><i class="fa-li fas fa-lightbulb"></i>Automated lighting</li>
  <li class="py-1"><i class="fa-li fas fa-tachometer-alt"></i>Smart Meter visualization and data analysis</li>
  <li class="py-1"><i class="fa-li fas fa-lock"></i>Security monitoring</li>
  <li class="py-1"><i class="fa-li fas fa-mobile-alt"></i>User interface on the touch screen of the {% include product-link.html product_name="Smart Building Gateway" %} as wekk as app access on moblie devices of the office employees</li>
  <li class="py-1"><i class="fa-li fas fa-chart-bar"></i> {% include product-link.html product_name="Smart Building Console" %} access for the facility management team</li>
</ul>

### <i class="fas fa-poll fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Result

Compared to the reference office wing, the energy usage was reduced by 30-34% in the winter months.

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/results.png" class="img-fluid py-3"  alt="{{page.name}}" />

For more information feel free to contact the [{{site.data.partners.iolite_iq.name}}]({{site.data.partners.iolite_iq.web}}){:target="\_blank"} team.
