---
layout: project
languages:
  - en
name: Smart Micro Grid
short_description: Testbed for e-mobility solutions of the future
city: Berlin
year: 2018
frontpage: false
partners:
  - dai
products:
  - IOLITE Platform
  - Smart Building Console
technologies:
  - Modbus
  - SNMP
  - Z-Wave
  - Davis weather station
  - OpenWeatherMap weather service
  - BMW
domains:
  - city
---

### <i class="fas fa-charging-station wow bounceIn p-2" data-wow-delay=".1s"/>Microgrid Testbed

The [{{site.data.partners.dai.name}}]({{site.data.partners.dai.web}}){:target="\_blank"} Smart Microgrid Testbed is located centrally in Berlin at the Telefunken building on the campus of the Technische Universität Berlin. It has been built as a testbed for research projects in the e-mobility and smart grid domain. It is continuously extended to present research results from projects to a wider public. It demonstrates the seamless integration of mobility, distributed energy production and storage into a smart sustainable living.

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/parking.png" class="img-fluid" alt="{{page.name}}" />

The focus of [{{site.data.partners.dai.name}}]({{site.data.partners.dai.web}}){:target="\_blank"}’s work in the testbed lies upon various goals. In addition to the overall goals of realizing optimized personal mobility for each e-vehicle user and, methods of non-discriminatory access to charging stations, one objective is to ensure electricity grid stability and support the integration of renewable energy sources in various e-mobility-scenarios. In this field of research, the testbed allows to undergo the process of technical realization, potential analysis and cost efficiency planning.

### <i class="fas fa-sitemap wow bounceIn p-2" data-wow-delay=".1s"/>Infrastructure

The testbed is built on an area of around 200 m²:

<ul class="fa-ul">
  <li class="py-1"><i class="fa-li fas fa-parking"></i>4 parking spaces equipped with parking lot sensors</li>
  <li class="py-1"><i class="fa-li fas fa-charging-station"></i>3 conductive charging stations (2 charge points each), 2 of which fully controllable by DAI-Labor</li>
  <li class="py-1"><i class="fa-li fas fa-car-battery"></i>1 Li-Ion battery (10,8 kWh)</li>
  <li class="py-1"><i class="fa-li fas fa-solar-panel"></i>1 Photovoltaic plant (8,8 kWp)</li>
  <li class="py-1"><i class="fa-li fas fa-car"></i>1 BMW i3 electric vehicle used for analysis, testing and integration</li>
  <li class="py-1"><i class="fa-li fas fa-tachometer-alt"></i>Detailed power quality analysis metering</li>
  <li class="py-1"><i class="fa-li fas fa-warehouse"></i>Parking gate for controlled access</li>
  <li class="py-1"><i class="fa-li fas fa-cloud-sun-rain"></i>Environmental sensors and a weather station</li>
</ul>

<div class="container">
  <div class="row ">
    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
      <img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/charging.jpg" class="img-fluid" alt="{{page.name}}" />
    </div>
    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 py-sm-3">
      <img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/main.jpg" class="img-fluid"  alt="{{page.name}}" />
    </div>
  </div>
</div>

### <i class="fas fa-sitemap wow bounceIn p-2" data-wow-delay=".1s"/>Integration

The {{page.name}} provides a good example of IOLITE's smart city integration capabilities. The testbed consists of **19 devices and sensors** with **157 data points** connected via **6 protocols / APIs**.

<p>The infrastructure is managed by the {% include product-link.html product_name="IOLITE Platform" %}, connecting all devices and sensors and making them interoperable for over-arching energy management use cases.</p>

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/ui.png" class="img-fluid"  alt="{{page.name}}" />

<p>Additionally the testebed is equipped with the {% include product-link.html product_name="Smart Building Console" %}. Grid operators benefit from the easy and intuitive user interface. A dashboard visualizes all devices and sensors at a glance. Additional views enable control and configuration of automation szenarios.</p>
