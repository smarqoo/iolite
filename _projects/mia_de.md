---
layout: project
languages:
  - de
name: Meine Intelligente Assistenz
short_description: 3000 Bestandswohnungen
city: Berlin
year: 2018
frontpage: true
partners:
  - iolite_iq
  - deuwo
  - jaeger_direkt
products:
  - Smart Building Gateway
  - IOLITE Platform
technologies:
  - EnOcean
  - Wireless M-Bus
  - Wetterdienst
  - LTE
domains:
  - home
---

### <i class="fas fa-paper-plane wow bounceIn p-2" data-wow-delay=".1s"/>Digitalisierung vom Wohnungsbestand

Im Herbst 2018 startete das Projekt _Meine Intelligente Assistenz_ (MiA) - die Ausstattung von bis zu 3.000 Bestandswohnungen der {{site.data.partners.deuwo.name}} in drei Bezirken Berlins. Die Ziele des Projektes waren:

- Einsparung von Heizenergie in den Immobilien
- Erhöhung des Wohnkomforts der Mieter
- Schaffen eines erweiterbaren, digitalen Interaktionspunktes in der Wohnung für Mehrwertdienste und Mieterkommunikation

Die größte Herausforderung des Projektes lag in der renovierungsfreien Nachrüstung vom teils denkmalgeschützten Immobilienbestand.

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/carl_legien.jpg" class="img-fluid"  alt="{{page.name}}" />
Credit Foto © {{site.data.partners.deuwo.name}}

### <i class="fas fa-leaf wow bounceIn p-2" data-wow-delay=".1s"/>Nachhaltig und wartungsfrei

<p>Die MiA Lösung wurde zusammen mit der <a href="{{site.data.partners.iolite_iq.web}}" target="_blank">{{site.data.partners.iolite_iq.name}}</a> auf Basis des {% include product-link.html product_name="Smart Building Gateway" %} entwickelt. Dank des integrierten Touch-Screens und LTE Moduls eignet sich das Gateway für Installation in Bestandsimmobilien mit keiner IT Infrastruktur. Kombiniert mit Energie-autarken EnOcean Komponenten emöglicht es eine kabellose Nachrüstung ohne Batterien.</p>

- Heizantriebe gewinnen ihre Energie aus dem warmen Wasser des Heizkörpers und kommunizieren per Funk mit dem Wohnungsgateway. Eine manuelle Verstellung per Drehring am Stellantrieb ist weiterhin möglich, sodass den Mietern die gewohnten Steuerungsmöglichkeiten nicht weggenommen werden.
- Lichtschalter erzeugen Funksignale beim mechanischen Tastenruck und funktionieren wie traditionelle Lichtschalter.
- Raumsensoren laden sich mithilfe von Solarzellen auf, sodass auch hier keien Batterien getauscht werden müssen.

### <i class="fa fa-wrench wow bounceI p-2" data-wow-delay=".1s"/>Installation in 40 Minuten

Gestützt von einem digitalen Logistikprozesses werden die MiA Gateways bereits in der Produktion für die Immobilien vorkonfiguriert. Entsprechend der Wohnungstypen und ihrer Raumanzahl werden fertige Installationssets vorbereitet, bei denen alle Komponenten bereits aufeinander abgestimmt sind und kein weiteres _Pairing_ erfordern. Die Installationszeit in der Bestandsimmobilie wird dadurch aufs Minimum reduziert und dauert typischerweise **40 bis 60 Minuten**.

Das Gateway wird auf einen bestehenden Licht- oder Jalousieschalter montiert. Die Installation erfordert keine IT Kenntnisse und wird von Elektrofachkräften nach einer sehr kurzen Schulung vorgenommen.

<p>Die Mieter interagieren mit der MiA direkt am {% include product-link.html product_name="Smart Building Gateway" %} Touchscreen oder per App auf ihren mobilen Endgeräten.</p>

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/main.jpg" class="img-fluid"  alt="{{page.name}}" />
Credit Foto © {{site.data.partners.deuwo.name}}

### <i class="{{ site.data.usps.security.icon }} wow bounceIn p-2" data-wow-delay=".1s"></i>{% t 'usp.security.title' %}

Der Schutz der Privatsphäre der Bewohner hatte für alle Projektpartner oberste Priorität. Die MiA Lösung verarbeitet personenbeziehbare Daten nur in der Wohnung und ist komplett offline-fähig. Die Mobilfunkverbindung des Gateways wird für Updates und Übermittlung nicht-personenbezogenen Telemetriedaten genutzt. Weitere Daten werden nur bei expliziter Zustimmung der Nutzer transportiert - z.B. wenn Mieter die MiA mit der App auf ihren mobilen Endgeräten verknüpfen möchten oder im Support-Fall.

### <i class="fa fa-rocket wow bounceIn p-2" data-wow-delay=".1s"/>Zukunftssicher

<p>Die {% include product-link.html product_name="IOLITE Platform" %} garantiert eine effiziente Installation, Inbetriebnahme und den zukunftssicheren Betrieb inklusive Remote- und Update Management des Gateways, skalierbar für Massenmarkteinsatz. Das Gateway profitiert automatisch von neuen Funktionen der IOLITE Plattform, die als over-the-air Update installiert werden.</p>

Das integrierte Wireless M-Bus Modul ermöglicht die zukünftige Umsetzung von Smart Metering Szenarien. Für zusätzliche Hardwareerweiterungen steht ein USB-Port bereit.

### <i class="fas fa-file-alt fa-fw wow bounceIn p-2" data-wow-delay=".1s"/>Pressemitteilung

Aus der offiziellen [Pressemitteilung der {{site.data.partners.deuwo.name}}](https://www.deutsche-wohnen.com/ueber-uns/presse/pressemitteilungen/mia-meine-intelligente-assistenz-zieht-in-3000-haushalte-der-deutsche-wohnen-ein/){:target="\_blank"}:

**MiA – Meine intelligente Assistenz zieht in 3.000 Haushalte der Deutsche Wohnen ein**

_57% der Deutschen sind davon überzeugt, dass Smart Home-Anwendungen in ein paar Jahren in jedem Haushalt zu finden sein werden (Quelle: Bitkom Research 2018). Die Deutsche Wohnen teilt diese Einschätzung und wird in einem großflächig angelegten Pilotprojekt rund 3.000 Haushalte mit einem eigenen Smart Home-System ausstatten._

_Durch MiA – Meine intelligente Assistenz lässt sich individuell die Raumtemperatur in jedem einzelnen Zimmer der Wohnung einstellen. Über das Display kann nicht nur die Raumtemperatur, sondern auch die Uhrzeit, zu der die gewünschte Temperatur erreicht werden soll, eingestellt werden. Auf Wunsch besteht die Möglichkeit, das Tablet in ein vorhandenes WLAN des Mieters einzubinden, so dass dieser die Heizung auch von unterwegs per App steuern kann. Doch die Heizungssteuerung ist erst der Anfang. Zum Einen lässt sich MiA – Meine intelligente Assistenz individuell um weitere smarte Anwendungen erweitern, wie etwa Lichtsteuerung oder marktübliche Sprachsteuerungsgeräte. Zum Anderen wird die Deutsche Wohnen je nach Bedarf und Nachfrage MiA zukünftig mit weiteren, smarten Service-Funktionen ausstatten, die den Kunden das Wohnen erleichtern._

[Vollständiger Text der Pressemitteilung](https://www.deutsche-wohnen.com/ueber-uns/presse/pressemitteilungen/mia-meine-intelligente-assistenz-zieht-in-3000-haushalte-der-deutsche-wohnen-ein/){:target="\_blank"}

### <i class="fa fa-handshake wow bounceIn p-2" data-wow-delay=".1s"/>Vertrieb & Projektierung

Das [{{site.data.partners.iolite_iq.name}}]({{site.data.partners.iolite_iq.web}}){:target="\_blank"} Joint Venture bietet die MiA Lösung als Komplettpaket für Digitalisierung vom Neubau und Bestandsimmobilien an:

- Intelligentes IOLITE Gateway mit vernetzten Komponenten
- Logistik und Installation
- Betrieb als auch 1<sup>st</sup>- und 2<sup>nd</sup>-Level Support
- Ein Ökosystem von Mehrwertdiensten (z.B. Schadensmeldung, Mieterkommunikation)

Bei Interesse stellt Ihnen das Team der [{{site.data.partners.iolite_iq.name}}]({{site.data.partners.iolite_iq.web}}){:target="\_blank"} gerne weitere Informationen zur Verfügung.
