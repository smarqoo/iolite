---
layout: project
languages:
  - de
name: VKB Showroom
short_description: IoT Showroom der Versicherungskammer Bayern
city: München
map: Muenchen
year: 2018
frontpage: false
partners:
  - vkb
  - cl
  - kimocon
products:
  - IOLITE Platform
  - Smart Building Console
technologies:
  - EnOcean
  - Nuki
  - KNX
  - HomeMatic
  - Netatmo
  - Philips hue
  - Wetterdienst
  - D-Link
  - Sonos
  - Nest
  - Miele@Home
  - Alexa
domains:
  - home
---

### <i class="fas fa-paper-plane wow bounceIn p-2" data-wow-delay=".1s"/>Smart Home zum Anfassen

Das Ziel des Projektes war es, das digital vernetzte Leben für Mitarbeiter, Kunden und Partner der {{site.data.partners.vkb.name}} erlebbar zu machen und zu zeigen, wie die Digitalisierung und intelligent vernetzte Technologien den Alltag der Menschen beeinflussen und erleichtern können. In unterschiedlichen Themenwelten wird das Thema Smart Home anfassbar gemacht. Der Raum wird für Schulungen, Vorführungen und Treffen mit Mitarbeitern und Außendienstlern der VKB genutzt.

### <i class="fas fa-sitemap wow bounceIn p-2" data-wow-delay=".1s"/>Ausstattung

<p>In über 20 Smart Home Szenarien verdeutlicht der Showroom das große Potenzial von Technologien in den Bereichen Energie, Gesundheit, Sicherheit, Komfort und altersgerechtes Leben. Die offene und interoperable {% include product-link.html product_name="IOLITE Platform" %} – die technologische Grundlage des Showrooms – ermöglicht, Endgeräte aller Hersteller problemlos zu verknüpfen und so ein ganzheitliches System zu schaffen.</p>

<p>Insgesamt wurden <strong>33 Geräte und Sensoren</strong> mit <strong>181 Datenpunkte</strong> auf Basis von <strong>11 Protokollen</strong> vernetzt. Trotz der technischen Komplexität lässt sich der Showroom komfortabel und intuitiv mithilfe der {% include product-link.html product_name="Smart Building Console" %} steuern.</p>

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/kitchen.jpg" class="img-fluid"  alt="{{page.name}}" />

### <i class="fas fa-rocket wow bounceIn p-2" data-wow-delay=".1s"/>Use Cases

Der Showroom wurde in typische Wohnbereiche aufgeteilt:

<ul class="fa-ul">
  <li class="py-1"><i class="fa-li fas fa-door-open"></i>Eingang - mithilfe eines Kontaktsensors wird der Zustand der Eingangstür überwacht. Nutzer werden gewarnt, wenn die Tür lange offen bleibt oder beim Verlassen der Wohnung nicht korrekt zugemacht wurde. Ein vernetztes Türschloss ermöglicht eine automatische Verriegelung der Tür und Kontrolle des Schlosszustandes, z.B. über durch eine Abfrage des Sprachsystems Alexa.</li>
  <li class="py-1"><i class="fa-li fas fa-tv"></i>Wohnzimmer & Arbeitsecke - Lichtszenarien, Sprachsteuerung, vorkonfigurierte Szenen, sorgen für Komfort und perfekte Stimmung. Präsenzmelder detektieren Aktivität und schalten ungenutzte Geräte aus. Während der Abwesenheit der Bewohner lösen unerwünschte Bewegungen Einbruchalarme aus.</li>
  <li class="py-1"><i class="fa-li fas fa-blender"></i>Küche - Sprachausgabe warnt vor nicht dicht geschlossener Kühlschranktür, Nutzung der Kaffeemaschine wird erkannt und die Bewohner über den fertigen Kaffee informiert. Wenn gewünscht können Familienangehörige über die erfolgte Morgenroutine informiert werden. Sollte irgnedwo Wasser austreten, schlägt der Wassersensor Alarm. Höhen-verstellbare Küchenmöbel adaptieren sich zu den Präferenzen der Nutzer und sorgen für komfortable Arbeitsbedingungen</li>
  <li class="py-1"><i class="fa-li fas fa-utensils"></i>Essensbereich - Bewegungsmelder und Wandschalter steuern Szenen, z.B. für das richtige Ambiente beim Abendessen.</li>
  <li class="py-1"><i class="fa-li fas fa-bed"></i>Schlafbereich - <i>Guten Morgen</i> und <i>Gute Nacht</i> Szenarien steuern Lichter in der Wohnung, CO<sub>2</sub> Sensorik warnt vor schlechter Luftqualität und empfiehlt wann man lüften soll.</li>
</ul>
