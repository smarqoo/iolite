---
layout: project
languages:
  - en
name: Meine Intelligente Assistenz
short_description: 3000 residential apartments
city: Berlin
year: 2018
frontpage: true
partners:
  - iolite_iq
  - deuwo
  - jaeger_direkt
products:
  - Smart Building Gateway
  - IOLITE Platform
technologies:
  - EnOcean
  - Wireless M-Bus
  - Weather Service
  - LTE
domains:
  - home
---

### <i class="fas fa-paper-plane wow bounceIn p-2" data-wow-delay=".1s"/>Digitalization of residential properties

The _Meine Intelligente Assistenz_ (MiA) project started in autumn 2018 - the digitalization of up to 3.000 existing apartments of {{site.data.partners.deuwo.name}} in three districts of Berlin. The goals of the project are:

- Saving heating energy in the properties
- Increasing the comfort of the tenants
- Creation of a digital interaction point in the apartment for added value services and communication with the tenants

The main challange of the project was the renovation-free installation in partially landmarked historic buildings.

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/carl_legien.jpg" class="img-fluid"  alt="{{page.name}}" />
Credit Photo © {{site.data.partners.deuwo.name}}

### <i class="fas fa-leaf wow bounceIn p-2" data-wow-delay=".1s"/>Sustainable and maintenance-free

<p>MiA is a joint development of <a href="{{site.data.partners.iolite_iq.web}}" target="_blank">{{site.data.partners.iolite_iq.name}}</a> and {{site.data.company.name}}, based on the {% include product-link.html product_name="Smart Building Gateway" %} -  a wall-mounted IoT gateway. The integrated touch-screen and LTE radio make it work even in buildings with zero IT infrastructure. Combined with energy-autonomous EnOcean components the MiA enabled a wireless and battery-free installation.</p>

- heating valves harvest energy from the heat of the heaters and communicate wirelessly with the gateway of the apartment. The heaters can be controlled both via the gateway's touchscreen and manually using a ring on the valves. This way the traditional usage patterns are retained.
- rocker switches transform the mechanical energy of a button press and work like typical light switches.
- room sensors gather energy using solar panels so no battery exchange is necessary.

### <i class="fa fa-wrench wow bounceI p-2" data-wow-delay=".1s"/>Setup in 40 minutes

Thanks to logistics specilized for IoT requirements, the MiA intallation packages are tailored to each apartment already during the production process. Depending on the type of apartment and the number of rooms, all components of the installation packages are pre-configured, so no further _pairing_ is necessary. This way the installation time on-premise is reduced to a minimum. A typical MiA setup in an existing residentail apartment takes **40-60 minutes**.

The gateway is installed on an existing lights switch. The setup requires no programming skills and is carried out by electricians after a very short training.

<p>The tenants interact with MiA directly via the {% include product-link.html product_name="Smart Building Gateway" %}'s touchscreen or using the app on the mobile device.</p>

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/main.jpg" class="img-fluid"  alt="{{page.name}}" />
Credit Photo © {{site.data.partners.deuwo.name}}

### <i class="{{ site.data.usps.security.icon }} wow bounceIn p-2" data-wow-delay=".1s"></i>{% t 'usp.security.title' %}

The data and privacy protection for the tenants had the highest priority for all project partners. MiA processes data on-premise, on the gateway in the apartment and works without internet connectivity. The LTE connection of the gateway is only used for updates and the transport of basic, non-personal telemetry data. Any other data is transferred only with an explicity opt-in of the tenants - e.g. if they wish to access MiA with an app on their mobile devices or for 2<sup>nd</sup>-level support purposes.

### <i class="fa fa-rocket wow bounceIn p-2" data-wow-delay=".1s"/>Future-proof

<p>The underlying {% include product-link.html product_name="IOLITE Platform" %} guarantees an efficient installation, setup and operations (including remote management and over-the-air-updates) of the gateway on a mass scale. The MiA gateway benefits from continuous updates of the platform, automatically distributed via over-the-air update infrastructure provided by the {{site.data.company.name}}.</p>

The integrated Wireless M-Bus radio module guarantees compatibility with the _open metering system_ (OMS). A USB port enables to extend the gateway with additional modules in the future.

### <i class="fas fa-file-alt fa-fw wow bounceIn p-2" data-wow-delay=".1s"/> Press release

From the official [press release of {{site.data.partners.deuwo.name}}](https://www.deutsche-wohnen.com/en/about-us/press/press-releases/mia-my-intelligent-assistant-is-moving-into-3000-deutsche-wohnen-households/){:target="\_blank"}:

**MiA - My intelligent Assistant is moving into 3,000 Deutsche Wohnen households**

**Berlin, 30 August 2018.**_57% of people in Germany are convinced that smart home applications will be found in every household within the next few years (source: Bitkom Research 2018). Deutsche Wohnen shares this expectation und is going to install its own smart home application in 3,000 households in a pilot project. From autumn 2018, Berlin tenants in the Hospital Estate in Pankow and the two UNESCO World Heritage estates, the Carl Legien Residential Estate and the White City Estate, will be able to control their heating with a tablet screen. MiA – My intelligent Assistant – is moving in with them. This innovative installation will make it possible for residents to control their heating in comfort using a user-friendly touch display on a tablet or via an app when they are not at home. The tablet will be installed in the entrance area of each flat. An existing socket or light switch will be used for this purpose, so complicated building work will not be necessary._

_MiA – My intelligent Assistant enables tenants to set the room temperature in every single room in the flat. Using the display, they can set not only the temperature but also the time at which this temperature should be achieved. If tenants so wish, the tablet can be integrated into their WLAN. This means that they can control the heating in their flat via an app when they are not at home. But controlling the flat’s heating is just the beginning. MiA – My intelligent Assistant can be individually programmed to include further smart applications, for example, controlling lighting or typical voice-activated devices in the home. Furthermore, depending on need and demand, Deutsche Wohnen will add further smart service functions to MiA that make life at home easier._

[Full text online](https://www.deutsche-wohnen.com/en/about-us/press/press-releases/mia-my-intelligent-assistant-is-moving-into-3000-deutsche-wohnen-households/){:target="\_blank"}

### <i class="fa fa-handshake wow bounceIn p-2" data-wow-delay=".1s"/> Contact for project development

The [{{site.data.partners.iolite_iq.name}}]({{site.data.partners.iolite_iq.web}}){:target="\_blank"} joint venture offers the MiA as an all-in-one carefree digitalization package for new ans existing properties:

- Customizable, pre-configured hardware packages based on the smart building gateway and IOLITE software
- Logistics and installation
- Operations as well as 1<sup>st</sup>- and 2<sup>nd</sup>-level support
- Added-value service eco-system (e.g. tenant communication, damage reporting)

For more information feel free to contact the [{{site.data.partners.iolite_iq.name}}]({{site.data.partners.iolite_iq.web}}){:target="\_blank"} team.
