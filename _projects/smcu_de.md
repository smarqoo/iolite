---
layout: project
languages:
  - de
name: Smart Micro Grid
short_description: Testumgebung für E-Mobility Lösungen der Zukunft
city: Berlin
year: 2018
frontpage: false
partners:
  - dai
products:
  - IOLITE Platform
  - Smart Building Console
technologies:
  - Modbus
  - SNMP
  - Z-Wave
  - Davis Wetterstation
  - OpenWeatherMap Wetterdienst
  - BMW
domains:
  - city
---

### <i class="fas fa-charging-station wow bounceIn p-2" data-wow-delay=".1s"/>Microgrid Testbed

Um das Konzept der Elektromobilität zukünftig großflächig einsetzbar zu machen, wurde vom [{{site.data.partners.dai.name}}]({{site.data.partners.dai.web}}){:target="\_blank"} auf dem Campus der Technischen Universität Berlin ein vollständiges _Smart Micro Grid_-Testbed errichtet, welches die Vernetzung von E-Fahrzeug, Ladeinfrastruktur, Nutzer und Stromnetz mittels neuster Informations- und Kommunikationstechnologien live darstellt und erprobt. Das Testbed am Ernst-Reuter-Platz stellt neben einer umfassenden E-Mobility-Infrastruktur diverse Soft- und Hardwarekomponenten für Projekte und Tests im Bereich Elektromobilität zur Verfügung.

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/parking.png" class="img-fluid" alt="{{page.name}}" />

Die Ziele des [{{site.data.partners.dai.name}}]({{site.data.partners.dai.web}}){:target="\_blank"} konzentrieren sich neben der Gewährleistung der individuellen Mobilität für den Nutzer, der Unterstützung seiner intermodalen Mobilitätsplanung und dem diskriminierungsfreien Zugang zu Ladestationen, auf die Sicherung der Netzstabilität im Verteilnetz und intelligentes Lademanagement, die Integration von erneuerbaren Energien. Neben der technischen Umsetzung und der Potenzialanalyse steht die Kosteneffizienz für potenzielle Marktteilnehmer (EVU’s, Mobilitätskunden, Dienstleister usw.) ebenfalls im Fokus des Testbeds.

### <i class="fas fa-sitemap wow bounceIn p-2" data-wow-delay=".1s"/>Infrastruktur

Das Testbed ist auf einer Fläche von ca. 200 m² aufgebaut:

<ul class="fa-ul">
  <li class="py-1"><i class="fa-li fas fa-parking"></i>4 Parkplätze mit Parkplatzsensorik</li>
  <li class="py-1"><i class="fa-li fas fa-charging-station"></i>3 Ladesäulen (mit je 2 Ladepunkten), 2 komplett steuerbar vom DAI-Labor</li>
  <li class="py-1"><i class="fa-li fas fa-car-battery"></i>1 Li-Ion Batterie (10,8 kWh)</li>
  <li class="py-1"><i class="fa-li fas fa-solar-panel"></i>1 PV Anlage (8,8 kWp)</li>
  <li class="py-1"><i class="fa-li fas fa-car"></i>1 BMW i3, integriert für Analyse und Tests</li>
  <li class="py-1"><i class="fa-li fas fa-tachometer-alt"></i>Präzise Zählerinfrastruktur für die komplette Anlage</li>
  <li class="py-1"><i class="fa-li fas fa-warehouse"></i>Steuerbare Parkplatzschranke</li>
  <li class="py-1"><i class="fa-li fas fa-cloud-sun-rain"></i>Umgebungssensorik und Wetterstation</li>
</ul>

<div class="container">
  <div class="row ">
    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
      <img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/charging.jpg" class="img-fluid" alt="{{page.name}}" />
    </div>
    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 py-sm-3">
      <img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/main.jpg" class="img-fluid"  alt="{{page.name}}" />
    </div>
  </div>
</div>

### <i class="fas fa-sitemap wow bounceIn p-2" data-wow-delay=".1s"/>Ausstattung

Insgesamt wurden **19 Geräte und Sensoren** mit **157 Datenpunkten** auf Basis von **6 Protokollen** vernetzt.

<p>Die Basis für die Integration bietet die {% include product-link.html product_name="IOLITE Platform" %}, welche alle Geräte und Sensoren protokoll-unabhängig vernetzt und für übergeordnete Use Cases interoperabel macht.</p>

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/ui.png" class="img-fluid"  alt="{{page.name}}" />

<p>Darüber hinaus wird in dem Testbed die {% include product-link.html product_name="Smart Building Console" %} verwendet. Nutzer des Testbeds erhalten einen komfortablen und intuitiven Zugang zu allen Sensoren und Geräten. Auf dem Dashboard lässt sich mit einem Blick der Zustand aller wesentlicher Microgrid Komponenten einsehen.</p>
