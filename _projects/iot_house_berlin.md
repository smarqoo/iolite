---
layout: project
languages:
  - de
name: IoT@Home
short_description: Intelligentes Einfamilienhaus
city: Berlin
year: 2017
frontpage: false
products:
  - IOLITE Platform
  - Smart Building Console
technologies:
  - KNX
  - EnOcean
  - Smart Meter
  - Photovoltaik
  - Z-Wave
  - Miele@Home
  - OpenSprinkler
  - Mobotix
  - Sonos
domains:
  - home
---

### <i class="fa fa-home wow bounceIn px-2" data-wow-delay=".1s"/>Ein Beispielhaus aus unserem Portfolio

In einem modernen Einfamilienhaus in Berlin beweist die [IOLITE Plattform](../products/iolite-platform_de.html) im täglichen Betrieb die Fähigkeit eine hoch-heterogene IoT Umgebung zu vernetzen und mit intelligenten Funktionen anzureichen.

Auf insgesamt 4 Etagen werden 15 Räume mit insgesamt **43 Sensoren** und **96 Aktoren** auf Basis von **8 Protokollen** interoperabel vernetzt.

<!-- raw html due to layout, impossible with markdown -->
<div class="container">
  <div class="row ">
    <div class="col-xl-4 col-lg-4 col-md-8 d-flex flex-column align-items-start justify-content-center">
      <img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/overview.jpg" class="img-fluid py-3"  alt="Example" />
    </div>
    <div class="col-xl-4 col-lg-6 col-md-6 d-flex flex-column justify-content-start align-items-start px-3">
      <h4><i class="fa fa-sitemap wow bounceIn px-2" data-wow-delay=".1s"></i>Vernetzte Komponenten:</h4>
      <ul>
        <li>Beleuchtung</li>
        <li>Fensterrollos</li>
        <li>Steckdosen</li>
        <li>Küchengeräte</li>
        <li>Bewässerungsanlage</li>
        <li>Alarmanlage</li>
        <li>Heizung</li>
        <li>PV Anlage</li>
        <li>Kameras</li>
        <li>Temperatur- und Feuchtesensorik</li>
        <li>Bewegungsmelder und Lichtsensoren</li>
        <li>Fenster- und Türkontakte</li>
        <li>Wetterstation</li>
        <li>Smart Meter</li>
        <li>Streaming Geräte</li>
      </ul>
    </div>
    <div class="col-xl-4 col-lg-6 col-md-6 d-flex flex-column justify-content-start align-items-start px-3">
      <h4><i class="fa fa-rocket wow bounceIn px-2" data-wow-delay=".1s"></i>Umgesetzte Use Cases:</h4>
      <ul class="fa-ul">
        <li><i class="fa-li fas fa-lightbulb"></i>Automatische Beleuchtung</li>
        <li><i class="fa-li fas fa-lock"></i>Sicherheitsüberwachung</li>
        <li><i class="fa-li fa fa-sun"></i>Beschattung</li>
        <li><i class="fa-li fas fa-bolt"></i>Unwetterschutz</li>
        <li><i class="fa-li fas fa-tachometer-alt"></i>Smart Metering</li>
        <li><i class="fa-li fas fa-solar-panel"></i>PV Energiemanagement</li>
      </ul>
    </div>
  </div>
</div>
