---
layout: project
languages:
  - de
name: S-Labs
short_description: Vernetztes Bürogebäude in Istanbul
city: Istanbul
year: 2018
frontpage: true
products:
  - IOLITE Platform
  - Smart Building Console
technologies:
  - EnOcean
  - KNX
  - Miele@Home
  - Wetterdienst
domains:
  - office
---

_{{page.name}}_ in Istanbul ist eine kreative Büroumgebung mit 22 Räumen auf über 750m².

Das Büro besteht aus Arbeitsräumen, Meetingräumen, Testbeds, einer Terasse, Küche und einem interaktiven Präsentationsbereich.

## Use Cases

Das Ziel des Projektes war es eine komfortable, kreative, Energie-effiziente und sichere Arbeitsumgebung zu schaffen.

### <i class="fas fa-lightbulb fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Kreative Arbeitsumgebung

- Überwachung der Luftqualität
- Optimierung der Raumtemperatur
- Intelligente Beleuchtung, inklusive der Signalisierung von Luftqualität und anderer wichtiger Ereignisse

### <i class="fas fa-leaf fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Energiemanagement

- Überwachung der Energieverbräuche
- Automatisierung der Beleuchtung
- Steuerung der Heiz- und Kühlanlage

### <i class="fas fa-lock fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Sicherheit

- Präsenzüberwachung
- Anomaliedetektion
- Benachrichtigung und Alarmierung

## Infrastruktur

_{{page.name}}_ verfügt über eine breite Palette von Sensoren und Geräten, primär über KNX und EnOcean angebunden.

<p>Die {% include product-link.html product_name="IOLITE Platform" %} wird in einer virtualisierten Containerumgebung auf einem Server vor Ort ausgeführt.</p>

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/meeting.jpg" class="img-fluid py-1"  alt="{{page.name}}" />

### Sensoren

Jeder Raum ist mit einer Reihe von Sensoren ausgestattet, die eine ganzheitliche Überwachung des Komforts ermöglichen:

- CO<sub>2</sub>
- Luftfeuchte
- Temperatur
- Helligkeit
- Bewegung / Präsenz

Zusätzlich wurden an allen Türen und Fenstern Kontaktsensoren, primär für Sicherheitsszenarien, installiert. Vernetzte Zähler sorgen für Transparenz beim Energieverbrauch und den entstehenden Kosten.

### Geräte

- Beleuchtung, inklusive RGB-LED
- Lüftung- und Kühlanlagen
- Heizung
- Küchengeräte
