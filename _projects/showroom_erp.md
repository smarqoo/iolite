---
layout: project
languages:
  - de
name: Smart Life Lab
short_description: Connected Living Showroom
city: Berlin
year: 2017
frontpage: false
partners:
  - cl
  - dai
products:
  - IOLITE Platform
technologies:
  - KNX
  - BACnet
  - EnOcean
  - Z-Wave
  - Netatmo
  - Miele@Home
  - Alexa
  - D-Link
  - Philips hue
  - Sonos
  - Smart Metering Language
  - Kimocon
  - Weather Service
domains:
  - home
---

Das _{{page.name}}_ wurde im Jahr 2006 vom Bundesministerium für Wirtschaft und Energie im Rahmen des Forschungsfördervorhabens _SerCHo_ (Service Centric Home), als auf dem Internet der Dinge (IoT)-basierende Musterwohnung ins Leben gerufen. Der Showroom wird im 15. Stock des Telefunken-Hochhauses am Ernst-Reuter-Platz vom Verein _Connected Living e.V._ betrieben und ermöglicht einer breiten Öffentlichkeit und Fachdelegationen, innovative Lösungen für das Vernetzte Leben in einer realen Wohnungsumgebung zu erleben.

### <i class="fas fa-rocket fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Use Cases

Die im _{{page.name}}_ integrierten Lösungen verdeutlichen das große Potential und die Möglichkeiten der Technologien und vernetzten Systeme in den Bereichen Energie, Gesundheit, Sicherheit, Komfort, altersgerechtes Leben und Mobilität. Der Showroom adressiert u. a. eine der größten Herausforderungen im Vernetzten Leben: unterschiedliche Kommunikationsstandards- und Protokolle, die eine herstellerübergreifende Vernetzung von Endgeräten verhindern.

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/living.jpg" class="img-fluid"  alt="{{page.name}}" />
Credit Foto © Thomas Bruns

<p>Die im {{page.name}} installierte {% include product-link.html product_name="IOLITE Platform" %} ermöglicht es, Endgeräte aller Hersteller problemlos zu verknüpfen. So konnte eine <i>State of the Art</i>-Umgebung für das Vernetzte Leben der Zukunft geschaffen werden, in der alle Connected Living-Vereinsmitglieder die Möglichkeit haben, ihre smarten Geräte und Lösungen in einem ganzheitlichen System zu vernetzen und der Öffentlichkeit zu präsentieren.</p>

### <i class="fas fa-sitemap fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Infrastruktur

Das _{{page.name}}_ besteht aus vier Räumen:

- Wohnzimmer
- Arbeitszimmer
- Schlafzimmer
- Küche

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/dining.jpg" class="img-fluid py-1"  alt="{{page.name}}" />
Credit Foto © Thomas Bruns

In der Wohnung werden **115 Geräte und Sensoren** mit insgesamt **355 Datenpunkten** auf Basis von **14 Protokollen** interoperabel vernetzt:

- Beleuchtung
- Fensterrollos
- Temperatur- und Feuchtesensorik
- Heizung
- Steckdosen
- Küchengeräte
- Alarmanlage
- PV Anlage
- Kameras
- Bewegungsmelder und Lichtsensoren
- Fenster- und Türkontakte
- Wetterstation
- Smart Meter
- Streaming Geräte

### <i class="fas fa-vr-cardboard fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Interaktion der Zukunft

Ein besonderer Fokus des Showrooms liegt auf innovativen Interaktionsmodalitäten.

<ul class="fa-ul">
  <li class="py-1"><i class="fa-li fas fa-person-booth"></i>Vernetzte Textilien, wie ein Fenstervorhang, der seinen Status erkennt, Lichteinstrahlung misst und Interaktion durch Berührung unterstützt</li>
  <li class="py-1"><i class="fa-li fas fa-ring"></i>Gestikinteraktion anhand innovativer Fingergestenerkennung in einem Ring, entwickelt am <a href="{{site.data.partners.dai.web}}" target="_blank">{{site.data.partners.dai.name}}</a> der Technischen Universität Berlin</li>
  <li class="py-1"><i class="fa-li fas fa-comments"></i>Sprachsteuerung mithilfe von modernen Sprachassistenten</li>
  <li class="py-1"><i class="fa-li fas fa-portrait"></i>Interaktiver Spiegel, der Zusatzinformationen anzeigt und mit Gestikinteraktion steuerbar ist</li>
  <li class="py-1"><i class="fa-li fa fa-vr-cardboard"></i>Augmented Reality ergänzt andere Interaktionsmodalitäten mit in die Umgebung verankerten Anzeigen</li>
</ul>

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/bedroom.jpg" class="img-fluid py-1"  alt="{{page.name}}" />
Credit Foto © Thomas Bruns

### <i class="fas fa-handshake fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Partnerschaft

Die {{site.data.company.name}} ist Mitglied des Vereins Connected Living und bietet den Partnern des Vereins die Integration mit der Showroom Infrastruktur. Für mehr Informationen kontaktieren Sie bitte die Geschäftsstelle des [{{site.data.partners.cl.name}}]({{site.data.partners.cl.web}}){:target="\_blank"}.
