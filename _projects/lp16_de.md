---
layout: project
languages:
  - de
name: LP16
short_description: 30% Energieeinsparung im Büro
city: Berlin
year: 2019
frontpage: false
partners:
  - iolite_iq
  - synvia
products:
  - IOLITE Platform
  - Smart Building Gateway
  - Smart Building Console
technologies:
  - EnOcean
  - KNX
  - Wetterdienst
  - WiFi
domains:
  - office
---

Im Rahmen des _LP16_ Projektes wurden in Kooperation mit der [{{site.data.partners.iolite_iq.name}}]({{site.data.partners.iolite_iq.web}}){:target="\_blank"} die Büroflächen der Hauptstadtrepräsentanz der {{site.data.partners.synvia.name}} am Leipziger Platz 16 in Berlin ausgestattet.

### <i class="fas fa-leaf fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Projektziel

Im Fokus der Nachrüstung lag eine nachweisbare Energieeinsparung, primär im Bereich der Heizenergie.

### <i class="fas fa-ruler-combined fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Planung und Ausstattung

Die Büroflächen verfügen über zwei identische Flügel. Daher wurde ein Flügel mit der IOLITE Technologie ausgestattet und der zweite Flügel als Referenz genutzt.

Die Beleuchtung, die Lüftungsanalage und Smart Meter wurden mit dem KNX Bussystem vernetzt.

Für eine nachhaltige und wartungsfreie Nachrüstung wurden Energie-autarke EnOcean Sensoren und Aktoren ausgesucht:

- Heizantriebe gewinnen ihre Energie aus dem warmen Wasser des Heizkörpers
- Lichtschalter erzeugen Funksignale beim mechanischen Tastenruck
- Fenster-Kontaktsensoren und Bewegungsmelder laden sich mithilfe von Solarzellen auf

<p>Die Integration aller Geräte, Sensoren und Dienste erfolgt in der {% include product-link.html product_name="IOLITE Platform" %}, welche auf einem {% include product-link.html product_name="Smart Building Gateway" %} in einem der Meeeting Räume installiert wurde.</p>

### <i class="fas fa-rocket fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Use Cases

<ul class="fa-ul">
  <li class="py-1"><i class="fa-li fas fa-thermometer-half"></i>Raumbasierte Heizungsautomation mit automatisierten Heizplänen pro Raum und Wochentag</li>
  <li class="py-1"><i class="fa-li fas fa-door-open"></i>Absenkung der Heizung bei Fenster-auf Erkennung und keiner Aktivität im Raum</li>
  <li class="py-1"><i class="fa-li fas fa-lightbulb"></i>Automatisierung der Beleuchtung</li>
  <li class="py-1"><i class="fa-li fas fa-tachometer-alt"></i>Anbindung von Smart Meter für Visualisierung und Datenauswertung</li>
  <li class="py-1"><i class="fa-li fas fa-lock"></i>Sicherheitsüberwachung</li>
  <li class="py-1"><i class="fa-li fas fa-mobile-alt"></i>Visualisierung auf dem Touch-Screen des Smart Building Gateways und als App auf mobilen Endgeräten der Benutzer</li>
  <li class="py-1"><i class="fa-li fas fa-chart-bar"></i> {% include product-link.html product_name="Smart Building Console" %} Zugang fürs Facility Management Team</li>
</ul>

### <i class="fas fa-poll fa-fw wow bounceIn py-2" data-wow-delay=".1s"/> Ergebnis

Im Vergleich zu der Referenz-Bürofläche konnte in den Wintermonaten eine Einergieeinsparung von 30-34% nachgewiesen werden.

<img src="{{ site.baseurl }}/img/portfolio/projects/{{page.name}}/results.png" class="img-fluid py-3"  alt="{{page.name}}" />

Bei Interesse stellt Ihnen das Team der [{{site.data.partners.iolite_iq.name}}]({{site.data.partners.iolite_iq.web}}){:target="\_blank"} gerne weitere Informationen zur Verfügung.
