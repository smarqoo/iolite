/*!
 * Start Bootstrap - Creative Bootstrap Theme (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

(function($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
    $("html, body")
      .stop()
      .animate(
        {
          scrollTop: $($anchor.attr("href")).offset().top - 40
        },
        1250,
        "easeInOutExpo"
      );
        event.preventDefault();
    });

    // changes the nav bar class so it is not transparent after scrolling down
    $(window).scroll(function() {
        $('.navbar-collapse').collapse('hide');
        $('nav').toggleClass('scrolled', $(this).scrollTop() > 400);
    });

    // make sure navbar menu is not transparent when expanded
    $('.nav').on('show.bs.collapse', function() {
        $('nav').addClass('scrolled');
    });
    
    // make sure navbar menu is back to transparent when collapsed
    $('.nav').on('hide.bs.collapse', function() {
        if ($(window).scrollTop() <= 400) {            
            $('nav').removeClass('scrolled');
        }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $('.nav-link').click(function() {
        $('.navbar-collapse').collapse('hide');
    });

    // Fit Text Plugin for Main Header
    $("h1").fitText(1.2, {
        minFontSize : '35px',
        maxFontSize : '65px'
    });

    // Initialize WOW.js Scrolling Animations
    new WOW().init();

})(jQuery); // End of use strict
