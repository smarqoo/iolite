---
layout: text
languages:
  - en
  - de
---

# Impressum

## Angaben gemäß § 5 TMG:

{{ site.data.company.name }}  
{{ site.data.company.street }}  
{{ site.data.company.postal-code }} {{ site.data.company.city }}

### Vertreten durch:

Geschäftsführung: Grzegorz Lehmann

### Kontakt:

Telefon: {{ site.data.company.phone }}  
E-Mail: {{ site.data.company.email }}

### Handelsregister:

Registergericht: Amtsgericht Charlottenburg  
Registernummer: HRB 169900 B

### Umsatzsteuer-ID:

DE302504184

### Verantwortlich für den Inhalt nach §55 Abs.2 RStV:

Grzegorz Lehmann
Bismarckstr. 10-12
10625 Berlin

### Quellenangaben für die verwendeten Bilder und Grafiken:

© OpenStreetMap-Mitwirkende (https://www.openstreetmap.org)

## Haftungsausschluss (Disclaimer):

### Haftung für Inhalte

Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen
verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder
gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit
hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen
bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten
Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend
entfernen.

### Haftung für Links

Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können
wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der
jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung
auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine
permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung
nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.

### Urheberrecht

Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die
Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen
der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den
privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt
wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet.
Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei
Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.

### Online-Streitbeilegung gemäß Art. 14 Abs. 1 der Verordnung über Online-Streitbeilegung in Verbraucherangelegenheiten (Verordnung (EU) Nr. 524/2013)

Die Europäische Kommission stellt eine Plattform zur Online-Streitbeilegung (sog. „OS-Plattform“) bereit. Die
OS-Plattform dient als Anlaufstelle zur außergerichtlichen Beilegung von Streitigkeiten betreffend vertraglichen
Verpflichtungen, die aus Online-Kaufverträgen bzw. Online-Dienstleistungsverträgen erwachsen.

Die OS-Plattform ist unter dem folgendem Link zu erreichen: http://ec.europa.eu/consumers/odr
